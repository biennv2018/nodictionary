package com.nguyenvanbien.nodictionary.subclass;

import android.content.Context;
import android.content.SharedPreferences;

public class SettingFileManager {
    private String mNameFile;
    public String[] mKey;

    public SettingFileManager() {
        mNameFile = "setting";
        mKey = new String[]{"announce", "quantity", "starttime", "endtime", "vibrate", "lock"};
    }

    public void setData(Context context, String[] datas) {
        SharedPreferences file = context.getSharedPreferences(mNameFile, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = file.edit();

        int i = -1;
        for(String key : mKey){
            i = i + 1;
            editor.putString(key, datas[i]);
        }
        editor.apply();
    }

    public String[] getData(Context context){
        SharedPreferences file = context.getSharedPreferences(mNameFile, Context.MODE_PRIVATE);
        String[] datas = new String[mKey.length];

        int i = -1;
        for(String key : mKey){
            i = i + 1;
            datas[i] = file.getString(key, null);
        }

        return datas;
    }

    public Boolean getAnnounceOnOf(Context context){
        SharedPreferences file = context.getSharedPreferences(mNameFile, Context.MODE_PRIVATE);

        String announce = file.getString(mKey[0], null);
        if(announce==null){
            return false;
        }

        return Boolean.parseBoolean(announce);
    }

    public int getQuantity(Context context){
        SharedPreferences file = context.getSharedPreferences(mNameFile, Context.MODE_PRIVATE);

        String quantity = file.getString(mKey[1], null);
        if(quantity==null){
            return 1;
        }

        return Integer.parseInt(quantity.substring(0, quantity.indexOf(" ")));
    }

    public String getTimeStart(Context context){
        SharedPreferences file = context.getSharedPreferences(mNameFile, Context.MODE_PRIVATE);

        return file.getString(mKey[2], null);
    }

    public String getTimeEnd(Context context){
        SharedPreferences file = context.getSharedPreferences(mNameFile, Context.MODE_PRIVATE);

        return file.getString(mKey[3], null);
    }

    public Boolean getVibrateOnOf(Context context){
        SharedPreferences file = context.getSharedPreferences(mNameFile, Context.MODE_PRIVATE);

        String vibrate = file.getString(mKey[4], null);
        if(vibrate==null){
            return false;
        }

        return Boolean.parseBoolean(vibrate);
    }

    public boolean getLockScreen(Context context){
        SharedPreferences file = context.getSharedPreferences(mNameFile, Context.MODE_PRIVATE);

        String lockScreen = file.getString(mKey[5], null);
        if(lockScreen==null){
            return false;
        }

        return Boolean.parseBoolean(lockScreen);
    }
}
