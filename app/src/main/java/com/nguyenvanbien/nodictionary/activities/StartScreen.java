/**************************************************************************************************
 * Hien thi giao dien chinh cua app
 * Chuc nang: tra tu, xem lai tu yeu thich, lich su, choi game, cai dat va thoat
 *************************************************************************************************/
package com.nguyenvanbien.nodictionary.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.nguyenvanbien.nodictionary.R;
import com.nguyenvanbien.nodictionary.adapter.AdtAutoCompleteTextView;
import com.nguyenvanbien.nodictionary.broadcast.UnlockScreen;
import com.nguyenvanbien.nodictionary.database.manager.ManagerDatabase;
import com.nguyenvanbien.nodictionary.database.models.DatabaseModel;
import com.nguyenvanbien.nodictionary.fragments.DisplayWordFragment;
import com.nguyenvanbien.nodictionary.fragments.MenuFragment;
import com.nguyenvanbien.nodictionary.interfaces.IAtcplTextviewComponents;
import com.nguyenvanbien.nodictionary.interfaces.IDisplayMenuScreen;

import java.util.ArrayList;
import java.util.List;

public class StartScreen extends AppCompatActivity implements TextWatcher, AdapterView.OnItemClickListener, IAtcplTextviewComponents, IDisplayMenuScreen{
    private static final int SET_DATA_CHANGE = 1;
    private static final int SET_TEXT_HINT = 2;
    //tu dien dang tra tu tieng anh hay tieng viet
    private Boolean mBlSearchInEn;
    //giao dien menu co dang duoc hien thi khong
    public Boolean mIsMenuDisplayed;
    //quan ly co so du lieu
    private ManagerDatabase mMngDatabase;
    //quan ly ban phim ao
    public static InputMethodManager mImmKeyboard;
    //nhap du lieu
    private List<String> mSuggesList;
    private static AutoCompleteTextView mAtcltvSearch;
    private AdtAutoCompleteTextView mAACTVSearch;
    private Handler mHandler;

    /**
     * ham khoi tao
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //hien thi layout menu
        setContentView(R.layout.start_screen);
        findViewById(R.id.lo_start_screen).startAnimation(AnimationUtils.loadAnimation(this, R.anim.alpha_0_1));
        findViewByIds();
        initComponents();
        setEvents();
    }

    /**
     * them button vao event
     */
    private void setEvents() {
        mAtcltvSearch.setOnItemClickListener(this);
        mAtcltvSearch.addTextChangedListener(this);
    }

    /**
     * khoi tao cac gia tri
     */
    private void initComponents() {
        mBlSearchInEn = true;
        mMngDatabase = new ManagerDatabase(this.getBaseContext(), this);
        mImmKeyboard = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        //danh sach du lieu
        initSearchWord();

        //khoi tao hanlder
        createHanler();

        //khoi tao fragment
        createMenuFragment();

    }

    private void initSearchWord() {
        mSuggesList = new ArrayList();
        //mArrAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, mSuggesList);
        ///mAtcltvSearch.setAdapter(mArrAdapter);
        mAACTVSearch = new AdtAutoCompleteTextView(
                getBaseContext(), R.layout.support_simple_spinner_dropdown_item, mSuggesList);
        mAtcltvSearch.setAdapter(mAACTVSearch);
    }

    /**
     * khoi tao hanler
     */
    private void createHanler() {
        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case SET_DATA_CHANGE:
                        List<String> datas = (List<String>)msg.obj;
                        if(datas!=null && datas.size()>0) {
                            mSuggesList.clear();
                            mSuggesList.addAll(datas);

                            //mAACTVSearch.clear();
                            //mAACTVSearch.addAll(datas);
                        }
                        mAACTVSearch.notifyDataSetChanged();
                        break;

                    case SET_TEXT_HINT:
                        mAtcltvSearch.setHint(msg.arg1);
                        break;

                    default:
                        break;
                }
            }
        };
    }

    /**
     * Tao fragment menu
     */
    private void createMenuFragment() {
        mIsMenuDisplayed = true;
        FragmentManager manager = this.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        MenuFragment menuFragment = new MenuFragment();
        //truyen gia tri
        menuFragment.setValueComponents(this, this, mMngDatabase);
        try {
            transaction.replace(R.id.frlo_container, menuFragment);
        }catch (Exception e) {
            transaction.add(R.id.frlo_container, menuFragment, MenuFragment.class.getName());
        }
        transaction.commit();
    }

    /**
     * anh xa id
     */
    private void findViewByIds() {
        mAtcltvSearch = (AutoCompleteTextView) findViewById(R.id.atcltv_search);
    }

    // xu ly su kien textWatch
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //nhan text

        String word = mAtcltvSearch.getText().toString();

        if (word.equals("")) {
            return;
        }

        //nhan lai du lieu thay doi
        if (mBlSearchInEn) {
            //tim trong bang tieng anh
            mMngDatabase.searchInEnglishTable(word);
        } else {
            //tim trong bang tieng viet
            mMngDatabase.searchInVietNameTable(word);
        }

    }

    @Override
    public void afterTextChanged(Editable s) {
        mAtcltvSearch.showDropDown();
    }

    /**
     * xu ly su kien onItemClickListional
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //an ban phim
        hideKeyBoard();

        //thong bao menu khong duoc hien thi
        mIsMenuDisplayed = false;

        //nhan du lieu tim kiem, loai tu va ten bang
        String word = mAtcltvSearch.getText().toString();
        word = word.toLowerCase();
        String nameTable;
        String type;
        if (mBlSearchInEn) {
            type = "en";
            nameTable = new DatabaseModel().getEnglishWordTable().getNameOfTable();
        } else {
            type = "vi";
            nameTable = new DatabaseModel().getVietNamWordTable().getNameOfTable();
        }

        //hien thi ket qua
        startDisplayWodFragment(word, nameTable);

        //them tu vao lich su tra tu
        mMngDatabase.insertWordInTable(word, type, new DatabaseModel().getHistoryTable().getNameOfTable());

        //xoa tu vua nhap
        mAtcltvSearch.setText("");
    }

    public static void hideKeyBoard(){
        if(mImmKeyboard.isAcceptingText()) {
            mImmKeyboard.hideSoftInputFromWindow(mAtcltvSearch.getWindowToken(), 0);
        }
    }

    /**
     * tao fragment
     * @param word
     * @param nameTable
     */
    private void startDisplayWodFragment(String word, String nameTable) {
        //nhan fragment da tap
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        DisplayWordFragment displayWordFragment = (DisplayWordFragment)
                manager.findFragmentByTag(
                DisplayWordFragment.class.getName());

        if( displayWordFragment == null ){
            //tao moi
            displayWordFragment = new DisplayWordFragment();
            //khoi tao du lieu
            displayWordFragment.initComponents(mMngDatabase, mBlSearchInEn, this);
            Bundle bundle = new Bundle();
            bundle.putString("word", word);
            bundle.putString("nametable", nameTable);
            displayWordFragment.setArguments(bundle);
        }else {
            //truyen du lieu hien thi
            displayWordFragment.setDataChange(mMngDatabase.getWord(word, nameTable));
        }

        transaction.replace(R.id.frlo_container, displayWordFragment,
                DisplayWordFragment.class.getName());
        transaction.commit();
    }


    /************************************
     * interface IAtcplTextViewCompontents
     ************************************/

    /**
     * @param listDatas du lieu truyen vao
     */
    @Override
    public void setDataChange(List<String> listDatas) {
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }

        Message message = new Message();
        message.what = SET_DATA_CHANGE;
        message.obj = listDatas;
        mHandler.sendMessage(message);

        //Looper.loop();     }

    }

    @Override
    public void setTextHintTextView(int textHint) {
        if (Looper.myLooper() == null){
            Looper.prepare();
        }
        Message message = new Message();
        message.what = SET_TEXT_HINT;
        message.arg1 = textHint;
        mHandler.sendMessage(message);
        //Looper.loop();
    }

    /**
     * gan gia tri cho bien
     * Neu tim kiem trong tieng anh thi gan true
     * Neu tim kiem trong tieng viet thi gan false
     * @param bool
     */
    @Override
    public void setSearchInEnOrVi(Boolean bool) {
        mBlSearchInEn = bool;
    }

    /**
     * tra ve tru neu dang tim kiem tu tieng anh
     * tra ve false neu dang tim kiem tu tieng viet
     * @return
     */
    @Override
    public Boolean getSearchInEnOrVi() {
        return mBlSearchInEn;
    }

    /****************************************
     * Event ISetDisplay menu
     * @param bool
     ***************************************/
    @Override
    public void setIsMenuDisplayed(Boolean bool) {
        mIsMenuDisplayed = bool;
    }

    @Override
    public void displayMenuScreen() {
        createMenuFragment();
    }

    @Override
    public void onBackPressed() {
        if( mIsMenuDisplayed ) {
            //menu duoc hien thi
            //goi ham back mac dinh
            super.onBackPressed();
        }else {
            createMenuFragment();
        }
    }

    @Override
    protected void onDestroy() {
        mMngDatabase.closeDatabase();
        super.onDestroy();
    }

}
