/*************************************************************************************************
 * Hien thi giao dien load du lieu
 ************************************************************************************************/
package com.nguyenvanbien.nodictionary.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.Toast;

import com.nguyenvanbien.nodictionary.R;
import com.nguyenvanbien.nodictionary.database.manager.CreateDatabase;
import com.nguyenvanbien.nodictionary.database.models.DatabaseModel;

import java.io.File;

public class LoadingDatabaseScreen extends AppCompatActivity implements Runnable{
    private static final String TAG = LoadingDatabaseScreen.class.getSimpleName();
    //ket qua cua loading
    private Boolean mLoading;
    //handler update giao dien va khoi tao activity
    private Handler mHandler;
    private ImageView mIvLoading;
    //thread load database
    private Thread thread;
    //tham so truyen tao database
    private Context mContext;
    //interface tra ve trang thai load du lieu
    private ILoadingData mIloadingData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_data);

        checkDatas();
        findViewByIds();
        initComponents();
    }

    /**
     * khoi tao cac thanh phan cua class
     */
    private void initComponents() {
        mLoading = true;
        mContext = this.getBaseContext();

        //update giao dien
        //start activity
        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1: //xoay hinh anh loading
                        mIvLoading.setRotation(msg.arg1);
                        break;

                    case 2: //load du lieu
                        endLoadData(msg);
                        break;

                    case 3: //in thong bao
                        Toast.makeText(mContext, (String)msg.obj, Toast.LENGTH_SHORT).show();
                        break;

                    default:
                        break;
                }
            }
        };

        //thread xoay hinh anh loading
        new Thread(new Runnable() {
            @Override
            public void run() {
                int rotate = 0;
                while (mLoading){
                    Message msg = new Message();
                    msg.what= 1;
                    msg.arg1 = rotate;
                    msg.setTarget(mHandler);
                    msg.sendToTarget();

                    rotate = (rotate+10)%360;
                    SystemClock.sleep(50);
                }
            }
        }).start();

        //tra ve trang thai load du lieu
        mIloadingData = new ILoadingData() {
            @Override
            public boolean IsLoading() {
                return mLoading;
            }
        };

        //thread tao co so du lieu
        //new Thread(this).start();
        thread = new Thread(this);
        thread.start();
    }

    private void findViewByIds() {
        mIvLoading = (ImageView) findViewById(R.id.imv_loading);
    }

    /**
     * Xu ly khi load xong du lieu
     * load thanh cong mo giao dien menu ung dung
     * Load that bai in ra thong bao va ket thuc chuong trinh
     * @param msg thong diep truyen vao cac trang thai
     */
    private void endLoadData(Message msg) {
        if(msg.arg1==1) {
            //load du lieu thanh cong
            //hien thi giao dien menu
            Intent intent = new Intent(LoadingDatabaseScreen.this, StartScreen.class);
            startActivity(intent);
            mLoading = false;
            finish();
        }else {
            //load du lieu that bai
            Toast toast = Toast.makeText(mContext,
                    "Cảnh báo: Lỗi cơ sở dữ liệu\nVui lòng khởi động lại ứng dụng",
                    Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER|Gravity.BOTTOM, 0, -100);
            toast.show();

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mLoading = false;
                    //toast.cancel();
                    System.exit(0);
                }
            }, 4000);
        }
    }

    /**********************************************
     * kiem tra du lieu co ton tai hay bi loi khong
     * @return true neu du lieu san sang su dung
     * false neu khong ton tai hoac bi loi
     **********************************************/
    private boolean checkDatas() {
        DatabaseModel modelDb = new DatabaseModel();
        File file = new File(modelDb.getPath()+File.separator+modelDb.getNameOfDatabase());

        if(!file.exists() || file.length()<modelDb.getSize()){
            file.delete();
            return false;
        }

        return true;
    }

    @Override
    public void run() {
        //load du lieu
        CreateDatabase createDb = new CreateDatabase(mContext, mHandler, mIloadingData);
        Boolean bool = createDb.getmBoolResultLoadData();

        //ket thuc load du lieu
        //kiem tra ket qua load
        Message msg = new Message();
        msg.setTarget(mHandler);
        if(bool) {
            //hien thi activity menu
            //id load du lieu
            msg.what = 2;
            //ket qua load du lieu
            msg.arg1 = 1;
        }else{
            //in ra thong bao loi
            //id load du lieu
            msg.what = 2;
            //ket qua load du lieu
            msg.arg1 = 0;
        }
        msg.sendToTarget();
    }

    @Override
    public void onBackPressed() {
        mLoading = false;
        System.exit(0);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        mLoading = false;
        super.onDestroy();
    }

    /**
     * Truyen du lieu giua 2 class
     * Tra ve true neu dang loading du lieu
     * Tra ve false neu hoan tat hoac loi chuong trinh
     */
    public interface ILoadingData{
        boolean IsLoading();
    }

}
