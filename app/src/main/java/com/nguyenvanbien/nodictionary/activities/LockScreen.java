package com.nguyenvanbien.nodictionary.activities;

import android.app.KeyguardManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import com.nguyenvanbien.nodictionary.R;
import com.nguyenvanbien.nodictionary.broadcast.UnlockScreen;
import com.nguyenvanbien.nodictionary.database.manager.ManagerDatabase;
import com.nguyenvanbien.nodictionary.word_model.EnglishWord;
import java.util.List;
import java.util.Random;

public class LockScreen extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = LockScreen.class.getSimpleName()+">";
    private TextView mTvQuestion;
    private Button mBtnAnswer1, mBtnAnswer2;
    private List<EnglishWord> datas;
    private boolean mAnswered;
    private KeyguardManager.KeyguardLock mKl;
    private View mVwLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //an thanh thong bao
        hideNotifycationBar();

        setContentView(R.layout.lock_screen);

        findViewByIds();
        setEvents();
        inits();
        turnOffLockScreenDefault();

        mVwLayout = findViewById(R.id.lnlo_lock);
        mVwLayout.startAnimation(
                AnimationUtils.loadAnimation(this, R.anim.left_to_right));
    }

    private void turnOffLockScreenDefault() {

        KeyguardManager km = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        mKl = km.newKeyguardLock(KEYGUARD_SERVICE);
        mKl.disableKeyguard();
    }

    /**
     * khong hien thi thanh thong bao
     */
    private void hideNotifycationBar() {
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            //an thi khong duoc hien nen
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);

        }
    }

    private void inits() {
        datas = new ManagerDatabase(getBaseContext()).getWordUnlockScreen();
        mTvQuestion.setText(datas.get(0).mMean);
        //Log.d(TAG, datas.get(0).mMean);

        //dao dap an
        if(new Random().nextInt(10)%2==0){
            mBtnAnswer1.setText(datas.get(0).mWord);
            mBtnAnswer2.setText(datas.get(1).mWord);
        }else {
            mBtnAnswer1.setText(datas.get(1).mWord);
            mBtnAnswer2.setText(datas.get(0).mWord);
        }

        mAnswered = false;
    }

    private void setEvents() {
        mBtnAnswer1.setOnClickListener(this);
        mBtnAnswer2.setOnClickListener(this);
    }

    private void findViewByIds() {
        mTvQuestion = (TextView) findViewById(R.id.tv_question);
        mBtnAnswer1 = (Button) findViewById(R.id.btn_answer_1);
        mBtnAnswer2 = (Button) findViewById(R.id.btn_answer_2);
    }

    @Override
    public void onClick(View v) {

        if(mAnswered){
            return;
        }

        mAnswered = true;

        switch(v.getId()){
            case R.id.btn_answer_1:
                checkResult(1);
                break;

            case R.id.btn_answer_2:
                checkResult(2);
                break;

            default:
                break;
        }
    }

    private void checkResult(int clickPosition){
        String result;
        if(1==clickPosition){
            result = mBtnAnswer1.getText().toString();
        }else {
            result = mBtnAnswer2.getText().toString();
        }

        if(result.equals(datas.get(0).mWord)){
            if(1==clickPosition){
                mBtnAnswer1.setBackgroundResource(R.drawable.case0);
            }else {
                mBtnAnswer2.setBackgroundResource(R.drawable.case0);
            }

            Animation animation = AnimationUtils.loadAnimation(this, R.anim.bottom_to_top);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    finish();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            mVwLayout.startAnimation(animation);

        }else {
            if(1==clickPosition){
                mBtnAnswer1.setBackgroundResource(R.drawable.case1);
            }else {
                mBtnAnswer2.setBackgroundResource(R.drawable.case1);
            }

            mAnswered = false;
        }

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        UnlockScreen.UNLOCK = false;
        super.onDestroy();
    }
}
