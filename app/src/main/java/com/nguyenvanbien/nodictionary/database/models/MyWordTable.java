package com.nguyenvanbien.nodictionary.database.models;

/**
 * Created on 28/03/2017.
 */

public class MyWordTable {
    protected MyWordTable() {}

    public String getNameOfTable(){
        return "my_word";
    }

    public String getNameOfWordColumn(){
        return "word";
    }

    public String getNameOfTypeWordColumn(){
        return "Type_word";
    }
}
