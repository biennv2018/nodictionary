/**************************************************************************************************
 * Model database
 * Contain the data tables
 *************************************************************************************************/
package com.nguyenvanbien.nodictionary.database.models;

import android.os.Environment;

import java.io.File;

public class DatabaseModel {
    public DatabaseModel() {}

    /**
     * @return ten databasse
     */
    public String getNameOfDatabase(){
        return "dictionary_of_team_10.db";
    }

    public long getSize(){
        return 36962304;
    }

    public String getPath(){
        String separator = File.separator;
        String path = Environment.getDataDirectory().getPath()+separator
                +"data"+separator+"com.nguyenvanbien.nodictionary"
                +separator+"databases";

        return path;
    }

    public int getVersion(){
        return 1;
    }

    /**
     * @return thong tin bang EnglishWordTable
     */
    public EnglishWordTable getEnglishWordTable(){
        return new EnglishWordTable();
    }

    /**
     * @return thong tin bang VietNamWordTable
     */
    public VietNamWordTable getVietNamWordTable(){
        return new VietNamWordTable();
    }

    /**
     * @return thong tin bang myWordTable
     */
    public MyWordTable getMyWordTable(){
        return new MyWordTable();
    }

    public HistoryTable getHistoryTable(){
        return new HistoryTable();
    }
}
