/**************************************************************************************************
 * Khoi tao co so du lieu
 * Tao co so du lieu, tao cac bang
 * Them du leiu vao bang
 * Nang cap co so du lieu
 *************************************************************************************************/

package com.nguyenvanbien.nodictionary.database.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;

import com.nguyenvanbien.nodictionary.activities.LoadingDatabaseScreen;
import com.nguyenvanbien.nodictionary.database.models.DatabaseModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class CreateDatabase extends SQLiteOpenHelper{
    private static final String  TAG = CreateDatabase.class.getSimpleName();
    //ket qua tao co so du lieu, trang thai them du lieu
    private Boolean mBoolResultLoadData;
    //truy cap tai nguyen
    private Context mContext;
    //update giao dien
    private Handler mHandler;
    //interface nhan thong tin loading du lieu
    private LoadingDatabaseScreen.ILoadingData mILoadingData;

    /**
     * Constructor
     ** create database
     */
    public CreateDatabase(Context context, Handler handler, LoadingDatabaseScreen.ILoadingData iLoadingData) {
        super(context, new DatabaseModel().getNameOfDatabase(), null, new DatabaseModel().getVersion());
        initComponents(context, handler, iLoadingData);
        contructorDatas();
    }

    /**
     * kho tao du lieu
     * khi goi ham nay database se duoc tao
     * Goi den onCreate, onUpgrade
     */
    private void contructorDatas() {
        SQLiteDatabase db = this.getWritableDatabase();

        if(db==null || !db.isOpen()){
            mBoolResultLoadData = false;
        }
        db.close();
    }

    /**
     * khoi tao cao thanh phan
     * @param context
     */
    private void initComponents(Context context, Handler handler, LoadingDatabaseScreen.ILoadingData iLoadingData) {
        mBoolResultLoadData = true;
        mContext = context;
        mHandler = handler;
        mILoadingData = iLoadingData;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        sendMessageForHandler("Đang tạo cơ sở dữ liệu");

        //tao bang
        sendMessageForHandler("Đang tạo bảng dữ liệu");
        createTable(db);
        sendMessageForHandler("Tạo bảng dữ liệu hoàn tất");

        //chen du lieu
        sendMessageForHandler("Chèn dữ liệu vào bảng");
        insertDatas(db);
        sendMessageForHandler("Chèn dữ liệu hoàn tất");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        sendMessageForHandler("Update cơ sở dữ liệu");
    }

    /**
     * @return ket qau load du lieu
     */
    public boolean getmBoolResultLoadData() {
        return mBoolResultLoadData;
    }

    /**
     * Create Table
     */
    private void createTable(SQLiteDatabase db){
        DatabaseModel dbmd = new DatabaseModel();
        createEnglishWordTable(db, dbmd);
        createVietNameWordTable(db, dbmd);
        createMyWordTable(db, dbmd);
        createHistoryTable(db, dbmd);
    }

    /**
     * tao bao history
     * @param db
     * @param dbmd
     */
    private void createHistoryTable(SQLiteDatabase db, DatabaseModel dbmd) {
        //tao bang history
        String code = "create table if not exists " +
                dbmd.getHistoryTable().getNameOfTable() + " ( " +
                dbmd.getHistoryTable().getNameOfWordColumn() +
                " text not null primary key, "+
                dbmd.getHistoryTable().getNameOfTypeWordColumn()+
                " text not null );";
        db.execSQL(code);
    }

    /**
     * tao bao myWord
     * @param db
     * @param dbmd
     */
    private void createMyWordTable(SQLiteDatabase db, DatabaseModel dbmd) {
        //tao bang myword
        String code = "create table if not exists " +
                dbmd.getMyWordTable().getNameOfTable() + "(" +
                dbmd.getMyWordTable().getNameOfWordColumn() +
                " text not null primary key, "+
                dbmd.getMyWordTable().getNameOfTypeWordColumn()+
                " text not null ); ";
        db.execSQL(code);
    }

    /**
     * create englistword table
     */
    private void createEnglishWordTable(SQLiteDatabase db, DatabaseModel dbmd){
        String code = "create table if not exists "+
                dbmd.getEnglishWordTable().getNameOfTable() + " ( " +
                dbmd.getEnglishWordTable().getNameOfEnglishWordColumn() +
                " text not null, "+
                dbmd.getEnglishWordTable().getNameOfVietNamWordColumn() +
                " text not null,"+
                " primary key( "+dbmd.getEnglishWordTable().getNameOfEnglishWordColumn()+" ) );";
        db.execSQL(code);
    }

    /**
     * create vietnamword table
     */
    private void createVietNameWordTable(SQLiteDatabase db, DatabaseModel dbmd){
        //tao bang vietnamWord
        String code = "create table if not exists " +
                dbmd.getVietNamWordTable().getNameOfTable() + " (" +
                dbmd.getVietNamWordTable().getNameOfVietNamWordColumn() +
                " text not null primary key, "+
                dbmd.getVietNamWordTable().getNameOfNoSignVietNamWordColumn() +
                " text, "+
                dbmd.getVietNamWordTable().getNameOfEnglishWordColumn() +
                " text not null );";
        db.execSQL(code);
    }

    /**
     * them du lieu vao bang
     * @param db database them du lieu
     */
    private void insertDatas(SQLiteDatabase db) {
        //them du lieu
        insertDatasInEnglishTable(db);
        insertDatasInVietNameTable(db);
    }

    /**
     * Them du lieu vao bang tu viet nam
     */
    private void insertDatasInVietNameTable(SQLiteDatabase db) {
        //Lay ten bang, ten cot can them du lieu
        DatabaseModel dbmd = new DatabaseModel();
        String nameTable = dbmd.getVietNamWordTable().getNameOfTable();
        String nameVNColumn = dbmd.getVietNamWordTable().getNameOfVietNamWordColumn();
        String nameNoSignVNColumn = dbmd.getVietNamWordTable().getNameOfNoSignVietNamWordColumn();
        String nameEColumn = dbmd.getVietNamWordTable().getNameOfEnglishWordColumn();

        try {
            //mo file chua du lieu tieng anh
            InputStream in = mContext.getAssets().open("datas/data_va.dd");
            BufferedReader input = new BufferedReader(new InputStreamReader(in));

            //them tu vao database
            String datas = input.readLine();
            String arrDatas[] = new String[3];
            ContentValues contentValues = new ContentValues();

            //dang thuc hien load du lieu mo thuc hien vong lap nay
            while (datas!=null && mILoadingData.IsLoading()){
                try {
                    //Log.d(TAG, datas);
                    //tach du lieu lam 3 phan
                    //phan tieng anh, phan tieng viet co dau va khong dau
                    arrDatas = datas.split("#", 3);

                    //them du lieu vao doi tuong
                    contentValues.put(nameVNColumn, arrDatas[0]);
                    contentValues.put(nameNoSignVNColumn, arrDatas[1]);
                    contentValues.put(nameEColumn, arrDatas[2]);
                    //chen du lieu vao database
                    db.insert(nameTable, null, contentValues);

                    //do du lieu moi
                    datas = input.readLine();
                }catch (Exception e){
                    //e.printStackTrace();
                }
            }

            input.close();
            in.close();

        } catch (IOException e) {
            mBoolResultLoadData = false;
            e.printStackTrace();
        }
        sendMessageForHandler("Thên dữ liệu từ Việt-Anh hoàn tất");
    }

    /**
     * them du lieu vao bang tu tieng anh
     */
    private void insertDatasInEnglishTable(SQLiteDatabase db) {
        //lay ten bang, ten cot
        DatabaseModel dbmd = new DatabaseModel();
        String nameTable = dbmd.getEnglishWordTable().getNameOfTable();
        String nameEColumn = dbmd.getEnglishWordTable().getNameOfEnglishWordColumn();
        String nameVNColumn = dbmd.getEnglishWordTable().getNameOfVietNamWordColumn();

        try {
            //mo file chua du lieu tieng anh
            InputStream in = mContext.getAssets().open("datas/data_av.dd");
            BufferedReader input = new BufferedReader(new InputStreamReader(in));

            //them tu vao database
            String datas = input.readLine();
            String arrDatas[] = new String[2];
            ContentValues contentValues = new ContentValues();

            //dang thuc hien load du lieu moi thuc hien vong lap nay
            while (datas!=null && mILoadingData.IsLoading()){
                try {
                    //Log.d(TAG, datas);
                    //tach du lieu lam 2 phan
                    //phan tieng anh va phan tieng viet
                    arrDatas = datas.split("##", 2);

                    //them du lieu vao doi tuong
                    contentValues.put(nameEColumn, arrDatas[0]);
                    contentValues.put(nameVNColumn, arrDatas[1]);
                    //chen du lieu vao database
                    //kiem tra thanh cong hay that bai
                    db.insert(nameTable, null, contentValues);

                    //do du lieu moi
                    datas = input.readLine();
                }catch (Exception e){
                    //e.printStackTrace();
                }
            }

            input.close();
            in.close();

        } catch (IOException e) {
            mBoolResultLoadData = false;
            e.printStackTrace();
        }

        sendMessageForHandler("Thêm dữ liệu Anh-Việt hoàn tất");
    }

    /**
     * gui thong bao cho handler de update len giao dien
     * @param message thong diep
     */
    @NonNull
    private void sendMessageForHandler(String message) {
        Message msg = new Message();
        msg.what=3;
        msg.obj = message;
        mHandler.sendMessage(msg);
    }
}
