package com.nguyenvanbien.nodictionary.database.models;

/**
 * Created on 28/03/2017.
 */

public class HistoryTable {
    protected HistoryTable(){}

    public String getNameOfTable(){
        return "history";
    }

    public String getNameOfWordColumn(){
        return "word";
    }

    public String getNameOfTypeWordColumn(){
        return "type_word";
    }
}
