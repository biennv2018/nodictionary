/**************************************************************************************************
 * Model VNWord table
 *************************************************************************************************/
package com.nguyenvanbien.nodictionary.database.models;

/**
 * Created by hello on 23/03/2017.
 */

public class VietNamWordTable {

    protected VietNamWordTable() {
    }

    public String getNameOfTable(){
        return "viet_name_word";
    }

    public String getNameOfEnglishWordColumn() {
        return "english_word";
    }

    public String getNameOfVietNamWordColumn() {
        return "vietnam_word";
    }

    public String getNameOfNoSignVietNamWordColumn(){
        return "nosign_vietnam_word";
    }
}
