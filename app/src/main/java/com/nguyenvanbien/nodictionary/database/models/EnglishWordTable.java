/***************************************************************************************************
 * Model englistword table
 **************************************************************************************************/

package com.nguyenvanbien.nodictionary.database.models;

public class EnglishWordTable {
    protected EnglishWordTable() {

    }

    public String getNameOfTable(){
        return "english_word";
    }

    public String getNameOfEnglishWordColumn() {
        return "english_word";
    }

    public String getNameOfVietNamWordColumn() {
        return "vn_word";
    }
}
