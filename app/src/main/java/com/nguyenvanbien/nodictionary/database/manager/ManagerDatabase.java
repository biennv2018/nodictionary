/**************************************************************************************************
 * Quan ly co so du lieu
 * Chuc nang: them, sua, xoa va tim kiem thong tin
 *************************************************************************************************/
package com.nguyenvanbien.nodictionary.database.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Handler;
import android.widget.Toast;
import com.nguyenvanbien.nodictionary.database.models.DatabaseModel;
import com.nguyenvanbien.nodictionary.database.models.EnglishWordTable;
import com.nguyenvanbien.nodictionary.database.models.HistoryTable;
import com.nguyenvanbien.nodictionary.database.models.MyWordTable;
import com.nguyenvanbien.nodictionary.database.models.VietNamWordTable;
import com.nguyenvanbien.nodictionary.interfaces.IAtcplTextviewComponents;
import com.nguyenvanbien.nodictionary.word_model.EnglishWord;
import com.nguyenvanbien.nodictionary.word_model.Word;

import java.io.File;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

public class ManagerDatabase {
    public static final int LIMIT_THREAD = 3;
    public static final int LIMIT_WORD = 1000;
    private static final String TAG = ManagerDatabase.class.getSimpleName() + ":)";
    private String mLike = "%";
    private Context mContext;
    private int mIsSearching;
    private IAtcplTextviewComponents mSearching;
    //private SQLiteDatabase mDb;
    private String mValueSentToThread;
    private int temp = 0;

    public ManagerDatabase(Context context, IAtcplTextviewComponents iAtcplTextviewComponents) {
        initComponents(context, iAtcplTextviewComponents);
    }

    public ManagerDatabase(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * khoi tao cac thanh phan
     * @param context ngu canh cua activity
     */
    private void initComponents(Context context, IAtcplTextviewComponents iAtcplTextviewComponents) {
        mContext = context;
        mIsSearching = 0;
        mSearching = iAtcplTextviewComponents;
        //mDb = opendDatabase();
    }

    /**
     * them tu vao bang
     * Chi co them them vao bang MyWordTable va HistoryTable
     * @param word du lieu truyen vao, neu null xoa toan bo du lieu
     * @param nameTable ten bang can them
     */
    public void insertWordInTable(String word, String typeWord, String nameTable){
        DatabaseModel dbmd = new DatabaseModel();

        //xac dinh ten cot can them du lieu
        String nameWordColumn = "";
        String nameTypeWordColumn = "";
        //bang myWord
        MyWordTable myWordTable = dbmd.getMyWordTable();
        if(nameTable.equals(myWordTable.getNameOfTable())){
            nameWordColumn = myWordTable.getNameOfWordColumn();
            nameTypeWordColumn = myWordTable.getNameOfTypeWordColumn();
        }
        //bang historyTable
        HistoryTable historyTable = dbmd.getHistoryTable();
        if(nameTable.equals(historyTable.getNameOfTable())){
            nameWordColumn = historyTable.getNameOfWordColumn();
            nameTypeWordColumn = historyTable.getNameOfTypeWordColumn();
        }

        //xoa du lieu truoc khi them
        deleteWordInTable(word, nameTable);

        //them du lieu
        SQLiteDatabase db = opendDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(nameWordColumn, word);
            contentValues.put(nameTypeWordColumn, typeWord);
            db.insert(nameTable, null, contentValues);
        }catch (SQLiteException sqle){}

        //mot bang chi chua 200 tu
        //kiem tra gioi han du lieu
        String code = "select count(*) as sl from "+nameTable+" ;";
        Cursor cursor = db.rawQuery(code, null);
        cursor.moveToFirst();
        int sl = cursor.getInt(cursor.getColumnIndex("sl"));
        //xoa mot hang
        if( sl>200 ){
            //nhan hang dau dien duoc them vao
            code = "select "+nameWordColumn+" from "+nameTable+" limit 1;";
            cursor = db.rawQuery(code, null);
            String dataDelete = cursor.getString(cursor.getColumnIndex(nameWordColumn));

            //xoa du lieu
            code = "delele from "+nameTable+" where "+
                    nameWordColumn+" = '"+dataDelete+"' ;";
            db.execSQL(code);
        }

        db.close();
    }

    public void deleteWordInTable(String word, String nameTable){
        DatabaseModel dbmd = new DatabaseModel();
        String nameColumn;

        //xac dinh ten cot dieu kien
        MyWordTable myWordTable = dbmd.getMyWordTable();
        if( myWordTable.getNameOfTable().equals(nameTable) ){
            nameColumn = myWordTable.getNameOfWordColumn();
        }else {
            nameColumn = dbmd.getHistoryTable().getNameOfWordColumn();
        }

        //xoa tu
        String code;
        if(word !=null) {
            code = "delete from " + nameTable + " where " + nameColumn +
                    " = '" + word + "' ;";
        }else {
            //xoa toan bo bang
            code = "delete from " + nameTable + " ;";
        }

        SQLiteDatabase db = opendDatabase();
        db.execSQL(code);
        db.close();
    }

    /**
     * nhan danh sach tu trong bang lich su va tu yeu thich
     * @param nameTable ten bang
     * @return tra ve danh sach
     */
    public List<Word> getListWord(String nameTable){
        List<Word> wordList = new ArrayList<>();
        String nameColumn1, nameColumn2;

        MyWordTable myWordTable = new DatabaseModel().getMyWordTable();
        if(myWordTable.getNameOfTable().equals(nameTable)){
            nameColumn1 = myWordTable.getNameOfWordColumn();
            nameColumn2 = myWordTable.getNameOfTypeWordColumn();
        }else {
            HistoryTable historyTable = new DatabaseModel().getHistoryTable();
            nameColumn1 = historyTable.getNameOfWordColumn();
            nameColumn2 = historyTable.getNameOfTypeWordColumn();
        }

        String code = "select * from "+nameTable+" ;";
        SQLiteDatabase db = opendDatabase();
        Cursor cursor = db.rawQuery(code, null);
        int id1 = cursor.getColumnIndex(nameColumn1);
        int id2 = cursor.getColumnIndex(nameColumn2);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            wordList.add(new Word(cursor.getString(id1), cursor.getString(id2)));
            cursor.moveToNext();
        }

        //dao nguoc danh sach
        Word word;
        int length = wordList.size();
        for(int i=0; i<=length/2-1; i++){
            word = wordList.get(i);
            wordList.set(i, wordList.get(length-1-i));
            wordList.set(length-1-i, word);
        }

        db.close();
        return wordList;
    }

    /**
     * nhan mot tu ngau nhien trong bang tu yeu thich
     * @return tu duoc chon
     */
    public static EnglishWord getRandomEnglishWord(){
        DatabaseModel modelDb = new DatabaseModel();
        String path = modelDb.getPath()+File.separator+modelDb.getNameOfDatabase();
        SQLiteDatabase db = SQLiteDatabase.openDatabase(
                path, null, Context.MODE_PRIVATE);
        if(!db.isOpen()){
            return null;
        }

        //nhan mot tu trong bang yeu thich
        MyWordTable myWordTable = new DatabaseModel().getMyWordTable();
        String code = "select * from "+myWordTable.getNameOfTable()
                + " where "+myWordTable.getNameOfTypeWordColumn()+"='en' "
                +" order by random() limit 1;";
        Cursor cursor = db.rawQuery(code, null);
        if(cursor==null || cursor.getCount()==0){
            cursor.close();
            db.close();
            return null;
        }

        int columnId = cursor.getColumnIndex(myWordTable.getNameOfWordColumn());
        cursor.moveToFirst();
        String word = cursor.getString(columnId);
        cursor.close();

        //nhan nghia cua no
        EnglishWordTable englishTable = new DatabaseModel().getEnglishWordTable();
        code = "select * from "+englishTable.getNameOfEnglishWordColumn()
                +" where "+englishTable.getNameOfEnglishWordColumn()+" = '"+word+"' ;";
        cursor = db.rawQuery(code, null);
        if(cursor==null){
            cursor.close();
            db.close();
            return null;
        }
        cursor.moveToFirst();
        columnId = cursor.getColumnIndex(englishTable.getNameOfEnglishWordColumn());
        int column1Id = cursor.getColumnIndex(englishTable.getNameOfVietNamWordColumn());
        EnglishWord englishWord = new EnglishWord(
                cursor.getString(columnId), cursor.getString(column1Id));
        cursor.close();
        db.close();
        return englishWord;
    }
    /**
     * tra ve tu va nghia tong bang
     * @param word tu can tra ve
     * @param nameTable ten bang
     * @return mang chua tu va nghia
     */
    public String[] getWord(String word, String nameTable){
        String[] arr;

        //bang EnglishWord
        arr = getWordInEnglishTable(word, nameTable);
        if(arr!=null){
            //chuan hoa du lieu
            arr[1] = convertString(arr[1]);
            return arr;
        }

        arr = getWordInVietNameTable(word, nameTable);
        if( arr!=null){
            //chuan hoa du lieu
            arr[1] = convertString(arr[1]);
            return arr;
        }

        return null;
    }

    /**
     * nhan tu va nghia cua tu trong bang tu tieng viet
     * @param word
     * @param nameTable
     * @return
     */
    private String[] getWordInVietNameTable(String word, String nameTable) {
        VietNamWordTable vnTable = new DatabaseModel().getVietNamWordTable();
        if( vnTable.getNameOfTable().equals(nameTable)){
            String nameColumn1 = vnTable.getNameOfVietNamWordColumn();
            String nameColumn2 = vnTable.getNameOfNoSignVietNamWordColumn();
            String nameColumn3 = vnTable.getNameOfEnglishWordColumn();

            String code = "select * from "+nameTable+
                    " where "+nameColumn1+" = '"+word+
                    "' or "+nameColumn2+" = '"+word+
                    "' limit 1;";
            SQLiteDatabase db = opendDatabase();
            Cursor cursor = db.rawQuery(code, null);
            int column1Id = cursor.getColumnIndex(nameColumn1);
            int column2Id = cursor.getColumnIndex(nameColumn2);
            int column3Id = cursor.getColumnIndex(nameColumn3);
            cursor.moveToFirst();
            String[] datas = new String[]{cursor.getString(column1Id), "TV không dấu: "+
                    cursor.getString(column2Id)+"\n"+cursor.getString(column3Id)};
            db.close();
            return datas;
        }else {
            return null;
        }
    }

    /**
     * nhan tu va nghia cua tu trong bang tieng anh
     * @param word
     * @param nameTable
     * @return
     */
    private String[] getWordInEnglishTable(String word, String nameTable) {
        EnglishWordTable eTable = new DatabaseModel().getEnglishWordTable();
        if( eTable.getNameOfTable().equals(nameTable) ) {
            String nameColumn1 = eTable.getNameOfEnglishWordColumn();
            String nameColumn2 = eTable.getNameOfVietNamWordColumn();

            String code = "select * from " + nameTable +
                    " where " + nameColumn1 + " = '" + word +
                    "' limit 1;";
            SQLiteDatabase db = opendDatabase();
            Cursor cursor = db.rawQuery(code, null);
            cursor.moveToFirst();
            int culumn1Id = cursor.getColumnIndex(nameColumn1);
            int culumn2Id = cursor.getColumnIndex(nameColumn2);

            String[] datas = new String[]{cursor.getString(culumn1Id), cursor.getString(culumn2Id)};
            db.close();
            return datas;
        }else {
            return null;
        }
    }

    /**
     * chuyen doi du lieu da ma hoa sang dang binh thuong
     * @param data
     * @return
     */
    private String convertString(String data){
        data = data.replace("#*","\n*");
        data = data.replace("#-","\n-");
        data = data.replace("|*","\n\n*");
        data = data.replace("|-","\n\t-");
        data = data.replace("|=","\n\t\t+ ");
        data = data.replace("|+","\n\t\t=> ");
        return data;
    }

    /**
     * tim kiem tu o trong bang englishWordTable
     * Ket qua toi da 100 tu
     * Ket qua khong bao gom nghia

     * @param word tu can tim kiem
     * @return danh sach cac tu
     */
    public void searchInEnglishTable(String word) {

        //neu dang tim kiem thi khong tim kiem nua
        if (mIsSearching == LIMIT_THREAD) {
            mSearching.setDataChange(null);
            return;
        }
        mValueSentToThread = word;
        //dang tim kiem du lieu
        mIsSearching = mIsSearching + 1;

        //tao luong tim du lieu
        new Thread(new Runnable() {
            @Override
            public void run() {
                //bang EnglishWordTable
                EnglishWordTable eWordTable = new DatabaseModel().getEnglishWordTable();
                String nameEColumn = eWordTable.getNameOfEnglishWordColumn();
                List<String> listDatas = new ArrayList<>();
                String code = "";

                //tim kiem du lieu gan giong voi tu duoc nhap
                code = "select " + nameEColumn + " from " +
                        eWordTable.getNameOfTable() + " where " +
                        nameEColumn;

                code = code + " like '" + mValueSentToThread + mLike + "' ";
                code = code + " limit " + LIMIT_WORD;
                code = code + " ;";

                getEnglishWordList(eWordTable, listDatas, code);

                //truyen gia tri de tim kiem
                mSearching.setDataChange(listDatas);
                //ket thuc tim kiem du lieu
                mIsSearching = mIsSearching - 1;
                temp = temp + 1;

            }
        }).start();

    }

    /**
     * kiem tra mot tu co trong bang tu cua toi hay khong
     * @param word tu can kiem tra
     * @return ket qua kiem tra
     */
    public boolean isWordLiked(String word) {
        MyWordTable myWordTable = new DatabaseModel().getMyWordTable();
        String code = "select * from "+myWordTable.getNameOfTable()+
                " where "+myWordTable.getNameOfWordColumn()+" = '"+
                word+"' limit 1;";

        SQLiteDatabase db = opendDatabase();
        Cursor cursor = db.rawQuery(code, null);
        int row = cursor.getCount();
        db.close();
        if(row == 0){
            return false;
        }else {
            return true;
        }
    }

    /**
     * lay du lieu tu cursor truyen vao list
     * @param eWordTable ten bang chua du lieu
     * @param listDatas danh sach chua du lieu
     * @param code ma sqlite
     */
    private void getEnglishWordList(EnglishWordTable eWordTable, List<String> listDatas, String code) {
        SQLiteDatabase db = opendDatabase();
        Cursor cursor = db.rawQuery(code, null);

        //lay id tu
        int wordId = cursor.getColumnIndex(eWordTable.getNameOfEnglishWordColumn());

        //lay du lieu
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            listDatas.add(cursor.getString(wordId));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
    }

    /******************************************
     * chuyen tieng viet co dau thanh khong dau
     * @param s
     * @return
     ******************************************/
    public static String removeAccent(String s) {

        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    /**
     * tim kiem tu o trong bang englishWordTable
     * Ket qua toi da 100 tu
     * Ket qua khong bao gom nghia
     * @param word tu can tim kiem
     * @return danh sach cac tu
     */

    public void searchInVietNameTable(String word) {
        //neu dang tim kiem thi khong tim kiem nua
        if (mIsSearching == LIMIT_THREAD) {
            mSearching.setDataChange(null);
            return;
        }
        //dang tim kiem du lieu
        mIsSearching = mIsSearching + 1;

        mValueSentToThread = removeAccent(word);

        new Thread(new Runnable() {
            @Override
            public void run() {
                //Thong tin bang
                VietNamWordTable vn = new DatabaseModel().getVietNamWordTable();
                String nameVnColumm = vn.getNameOfVietNamWordColumn();
                String nameNSVncolumn = vn.getNameOfNoSignVietNamWordColumn();

                //tim kiem thong tin
                String code = " select " + nameVnColumm + " , " + nameNSVncolumn +
                        " from " + vn.getNameOfTable() +
                        " where ";

                code = code + nameVnColumm + " like '" + mValueSentToThread + mLike +
                        "' or " + nameNSVncolumn + " like '" + mValueSentToThread + mLike +
                        "' ";
                code = code + " limit " + LIMIT_WORD;
                code = code + " ;";

                SQLiteDatabase db = opendDatabase();
                Cursor cursor = db.rawQuery(code, null);

                //lay thong tin
                List<String> listDatas = new ArrayList<>();
                int vnId = cursor.getColumnIndex(vn.getNameOfVietNamWordColumn());
                int noSignVnId = cursor.getColumnIndex(vn.getNameOfNoSignVietNamWordColumn());
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    String vietName = cursor.getString(vnId);
                    listDatas.add(vietName);
                    listDatas.add(cursor.getString(noSignVnId));
                    cursor.moveToNext();
                }
                cursor.close();
                db.close();

                //truyen gia tri de tim kiem
                mSearching.setDataChange(listDatas);

                mIsSearching = mIsSearching - 1;
            }
        }).start();

    }

    /*******************************
     * nhan du lieu mo khoa man hinh
     * tu thu nhat se la khoa
     * tu thu 2 khong co tac dung
     * @return du lieu
     ******************************/
    public List<EnglishWord> getWordUnlockScreen(){
        //lay 2 tu trong bao englise
        List<EnglishWord> datas = getTwoWordInEnglishTable();
        //tinh xac xuat thay the tu thu 2 bang tu trong bang lich su
        String historyWord = getOneWordInHistoryTable();
        if(historyWord!=null) {
            if (new Random().nextInt(100) % 2 == 0) {
                datas.get(1).mWord = historyWord;
                datas.get(1).mMean = null;
            }
        }

        //chuan hoa tu theo dinh dang
        //xoa phien am neu co
        String temp = datas.get(0).mMean;
        datas.get(0).mMean = temp.substring(temp.indexOf("]")+1);
        //chuan hoa dinh dang
        datas.get(0).mMean = convertString(datas.get(0).mMean);
        //xoa ky tu \n dau tien sau khi dinh dang
        datas.get(0).mMean = datas.get(0).mMean.substring(1);

        return datas;
    }

    /**
     * nhan 2 tu tu bang tu tieng anh
     * @return du lieu
     */
    private List<EnglishWord> getTwoWordInEnglishTable(){
        SQLiteDatabase db = opendDatabase();
        List<EnglishWord> datas = new ArrayList<>();

        EnglishWordTable enTable = new DatabaseModel().getEnglishWordTable();
        String codeSql = " select * from "+enTable.getNameOfTable()
                +" order by random() limit 2";
        Cursor cursor = db.rawQuery(codeSql, null);
        int enIndex = cursor.getColumnIndex(enTable.getNameOfEnglishWordColumn());
        int viIndex = cursor.getColumnIndex(enTable.getNameOfVietNamWordColumn());

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            String word = cursor.getString(enIndex);
            String mean = cursor.getString(viIndex);

            datas.add(new EnglishWord(word, mean));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return datas;
    }

    /**
     * nhan mot tu tu bang lich su
     */
    private String getOneWordInHistoryTable(){
        List<Word> wordList = getListWord(new DatabaseModel().getHistoryTable().getNameOfTable());
        if(wordList.size()==0){
            return null;
        }
        //nhan mot tu ngau nhien
        Random random = new Random();
        Word word;
        int count = 0;
        do {
            count += 1;
            word = wordList.get(random.nextInt(wordList.size()));
        }while (!word.mType.equals("en") && count<11 );

        return word.mWord;
    }

    /**************************
     * mo database
     * @return database duoc mo
     **************************/
    private SQLiteDatabase opendDatabase() {
        //mo database
        SQLiteDatabase db = mContext.openOrCreateDatabase(
                new DatabaseModel().getNameOfDatabase(), Context.MODE_PRIVATE, null
        );

        //thong bao loi va ket thuc chuong tinh
        if(db==null || !db.isOpen() ){
            Toast.makeText(mContext, "Không thể mở database", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    System.exit(2);
                }
            }, 2500);
        }

        return db;
    }

    public void closeDatabase(){
        //mDb.close();
    }

}
