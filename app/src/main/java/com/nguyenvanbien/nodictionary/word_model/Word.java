/************************************************************************************************
 * Mo hinh hinh mot tu co ban: tu va loai tu
 ***********************************************************************************************/

package com.nguyenvanbien.nodictionary.word_model;

public class Word {
    public String mWord;
    public String mType;

    public Word(String word, String type) {
        mWord = word;
        mType = type;
    }

}
