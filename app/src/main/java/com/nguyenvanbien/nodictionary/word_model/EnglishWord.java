package com.nguyenvanbien.nodictionary.word_model;

public class EnglishWord extends Word {
    public String mMean;

    public EnglishWord(String word, String mean) {
        super(word, "en");
        mMean = mean;
    }
}
