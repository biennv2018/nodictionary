package com.nguyenvanbien.nodictionary.applications;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.nguyenvanbien.nodictionary.broadcast.UnlockScreen;
import com.nguyenvanbien.nodictionary.services.LockService;
import com.nguyenvanbien.nodictionary.services.NotifyManager;
import com.nguyenvanbien.nodictionary.subclass.SettingFileManager;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        startMyServices(getBaseContext());
    }

    /**
     * bat dau service
     * @param context
     */
    public static void startMyServices(Context context) {
        //start service thong bao tu
        //ngung service neu no dang chay
        Intent anncounceService = new Intent(context, NotifyManager.class);
        context.stopService(anncounceService);
        //neu nguoi dung khong nhan thong bao
        //thi khong bat dau service
        SettingFileManager settingFileManager = new SettingFileManager();
        if(settingFileManager.getAnnounceOnOf(context)){
            context.startService(anncounceService);
        }

        //start service khoa man hinh
        Intent lockService = new Intent(context, LockService.class);
        context.startService(lockService);
    }
}
