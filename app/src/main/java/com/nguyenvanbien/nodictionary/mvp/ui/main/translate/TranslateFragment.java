package com.nguyenvanbien.nodictionary.mvp.ui.main.translate;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.nguyenvanbien.nodictionary.R;
import com.nguyenvanbien.nodictionary.activities.StartScreen;
import com.nguyenvanbien.nodictionary.interfaces.IDisplayMenuScreen;

import rx.internal.operators.CachedObservable;

public class TranslateFragment extends Fragment implements View.OnClickListener{
    private static final String TAG = TranslateFragment.class.getSimpleName() + ">";
    private EditText mEdtInput;
    private TextView mTvOutput;
    private IDisplayMenuScreen mIDisplayMenuScreen;
    private TranslatePresenter mTranslatePresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.translate, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViewByIds(view);
        setEvents(view);
        inits();
    }

    public void setValue(IDisplayMenuScreen iDisplayMenuScreen){
        mIDisplayMenuScreen = iDisplayMenuScreen;
    }

    private void inits() {
        mTranslatePresenter = new TranslatePresenter(mTvOutput);
    }

    private void setEvents(View view) {
        view.findViewById(R.id.btn_convert_en_vi).setOnClickListener(this);
        view.findViewById(R.id.btn_convert_vi_en).setOnClickListener(this);
        view.findViewById(R.id.imgbtn_back).setOnClickListener(this);

        view.findViewById(R.id.tv_result_title).setOnClickListener(this);
        view.findViewById(R.id.scrv_translate).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                StartScreen.hideKeyBoard();
                return false;
            }
        });
    }

    private void findViewByIds(View view) {
        mEdtInput = (EditText) view.findViewById(R.id.edt_word_input);
        mTvOutput = (TextView) view.findViewById(R.id.tv_word_output);
    }

    /**************************
     * su kien onClickListener
     * @param v
     **************************/
    @Override
    public void onClick(View v) {
        StartScreen.hideKeyBoard();

        String word = mEdtInput.getText().toString();
        switch (v.getId()){
            case R.id.btn_convert_en_vi:
                translateText(word, "vi");
                break;

            case R.id.btn_convert_vi_en:
                translateText(word, "en");
                break;

            case R.id.imgbtn_back:
                mIDisplayMenuScreen.displayMenuScreen();
                break;

            default:
                break;
        }
    }

    /**
     * dich tư
     * @param text
     * @param lang
     */
    private void translateText(String text, String lang){
        if(!checkInternet()){
            mTvOutput.setTextColor(Color.RED);
            mTvOutput.setText("Lỗi: Không có kết nối Internet...\nVui lòng thử lại");
            return;
        }

        mTvOutput.setTextColor(Color.BLACK);
        mTvOutput.setText("Đang dịch...");
        //Log.d(TAG, "text="+text+";");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTranslatePresenter.translateText(text, lang);
            }
        }, 100);
    }

    private boolean checkInternet(){
        ConnectivityManager connectMng = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo info = connectMng.getActiveNetworkInfo();

        if(info!=null && info.isConnected()){
            return true;
        }

        return false;
    }

    @Override
    public void onDestroy() {
        mTranslatePresenter.onDestroy();
        super.onDestroy();
    }
}
