package com.nguyenvanbien.nodictionary.mvp.ui.main.translate;

import android.graphics.Color;
import android.util.Log;
import android.widget.TextView;
import com.nguyenvanbien.nodictionary.mvp.interact.AccountInteractor;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class TranslatePresenter {
    private static final String TAG = TranslatePresenter.class.getSimpleName() +">";
    private TextView mTvMean;
    private Disposable mDisposable;

    public TranslatePresenter(TextView view) {
        mTvMean = view;
    }

    public void translateText(String text, String lang){
        //Log.d(TAG, "Đang dịch...");
        interact(AccountInteractor.getInstance().translateText(text, lang),
                response->{
                    mTvMean.setText(response.getmMean());
                    //Log.d(TAG, "MeanT="+ response.getmMean() +";");
                }, error -> {
                    String mean = error.getMessage();

                    mTvMean.setTextColor(Color.RED);
                    if(mean==null){
                        mTvMean.setText("Không thể kết nối tới server.\nVui lòng thử lại");
                    }else {
                        mTvMean.setText(mean);
                    }
                    //Log.d(TAG, "MeanF="+ mean +";");
                });
    }

    private <T> void interact(Observable<T> ob, Action<T> onNext, Action<Throwable> onError){
        if(mDisposable!=null && !mDisposable.isDisposed()){
            mDisposable.dispose();
        }

        mDisposable = ob.subscribe(response -> {
            onNext.call(response);
        }, error -> {
            onError.call(error);
        });
    }

    public void onDestroy(){
        if(mDisposable!=null && !mDisposable.isDisposed()){
            mDisposable.dispose();
        }
    }
}
