package com.nguyenvanbien.nodictionary.mvp.ui.main.translate;

public interface Action<E> {
    void call(E e);
}
