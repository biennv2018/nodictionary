package com.nguyenvanbien.nodictionary.mvp.interact;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.nguyenvanbien.nodictionary.mvp.module.TranslateTextResponse;
import java.util.concurrent.TimeUnit;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.nguyenvanbien.nodictionary.mvp.common.Constants.BASE_URL;
import static com.nguyenvanbien.nodictionary.mvp.common.Constants.KEY;

public class ApiConnector {
    private static ApiConnector instance = new ApiConnector();
    private ApiService mApiService;

    private ApiConnector() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS);

        Retrofit.Builder builder1 = new Retrofit.Builder();
        builder1.client(builder.build())
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());

        mApiService = builder1.build().create(ApiService.class);
    }

    static ApiConnector getInstance(){
        return instance;
    }

    public Observable<TranslateTextResponse> translateText(String text, String lang){
        return mApiService.translateText(text, KEY, lang)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
