package com.nguyenvanbien.nodictionary.mvp.module;

import com.google.gson.annotations.SerializedName;

public class TranslateTextResponse {
    @SerializedName("text")
    private String[] mMean;

    public TranslateTextResponse(String[] mMean) {
        this.mMean = mMean;
    }

    public String getmMean() {
        String mean = mMean[0];
        for(int i = 1; i<mMean.length; i++){
            mean = mean + "\n" + mMean[i];
        }
        return mean;
    }
}
