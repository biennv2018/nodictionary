package com.nguyenvanbien.nodictionary.mvp.common;

public interface Constants {
    String BASE_URL = "https://translate.yandex.net";
    String SUB_URL = "api/v1.5/tr.json/translate";
    String KEY = "trnsl.1.1.20170618T104932Z.7094c327a122ef67.a34996b3e53cecd91dedf650704946bc50750c5d";
}
