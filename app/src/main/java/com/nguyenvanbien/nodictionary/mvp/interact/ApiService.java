package com.nguyenvanbien.nodictionary.mvp.interact;
import com.nguyenvanbien.nodictionary.mvp.module.TranslateTextResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.nguyenvanbien.nodictionary.mvp.common.Constants.SUB_URL;

public interface ApiService {

    @GET(SUB_URL)
    Observable<TranslateTextResponse> translateText(@Query("text") String text,
                                                    @Query("key") String key,
                                                    @Query("lang") String lang);
}
