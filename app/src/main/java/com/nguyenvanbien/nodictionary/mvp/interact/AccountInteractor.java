package com.nguyenvanbien.nodictionary.mvp.interact;

import com.nguyenvanbien.nodictionary.mvp.module.TranslateTextResponse;

import io.reactivex.Observable;

public class AccountInteractor {
    private ApiConnector mApiConnector;
    private static AccountInteractor instance = new AccountInteractor();

    private AccountInteractor(){
        mApiConnector = ApiConnector.getInstance();
    }

    public static AccountInteractor getInstance(){
        return instance;
    }

    public Observable<TranslateTextResponse> translateText(String text, String lang){
        return mApiConnector.translateText(text, lang);
    }
}
