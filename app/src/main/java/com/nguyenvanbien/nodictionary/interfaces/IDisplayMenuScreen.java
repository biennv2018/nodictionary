package com.nguyenvanbien.nodictionary.interfaces;

public interface IDisplayMenuScreen {
    void setIsMenuDisplayed(Boolean bool);

    /**
     * Hien thi man hinh menu
     */
    void displayMenuScreen();
}
