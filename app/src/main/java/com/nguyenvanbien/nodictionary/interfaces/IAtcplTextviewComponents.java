/**************************************************************************************************
 * khoi tao lai du lieu cho autocompletetextview
 * khi du lieu thay doi
 *************************************************************************************************/
package com.nguyenvanbien.nodictionary.interfaces;

import java.util.List;

public interface IAtcplTextviewComponents {
    /**
     * du lieu thay doi yeu cau thay doi du lieu
     * @param listDatas du lieu truyen vao
     */
    void setDataChange(List<String> listDatas);

    /**
     * thay doi textHint cua text view
     * @param textHint
     */
    void setTextHintTextView(int textHint);

    /**
     * cai gai tri va nhan gia tri cho che do timf kiem
     * @param bool
     */
    void setSearchInEnOrVi(Boolean bool);
    Boolean getSearchInEnOrVi();

}
