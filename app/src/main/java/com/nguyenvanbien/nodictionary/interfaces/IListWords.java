package com.nguyenvanbien.nodictionary.interfaces;

import com.nguyenvanbien.nodictionary.word_model.Word;

public interface IListWords {
    int getCount();

    Word getItem(int position);

    void deleteItem(int position);

    void deleteAllItem();

    String[] getWord(String word, String nameTable);
}
