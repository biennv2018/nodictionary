package com.nguyenvanbien.nodictionary.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.nguyenvanbien.nodictionary.R;
import com.nguyenvanbien.nodictionary.activities.StartScreen;
import com.nguyenvanbien.nodictionary.database.manager.ManagerDatabase;
import com.nguyenvanbien.nodictionary.database.models.DatabaseModel;
import com.nguyenvanbien.nodictionary.dialogs.SetDialog;
import com.nguyenvanbien.nodictionary.interfaces.IAtcplTextviewComponents;
import com.nguyenvanbien.nodictionary.interfaces.IDisplayMenuScreen;
import com.nguyenvanbien.nodictionary.mvp.ui.main.translate.TranslateFragment;

public class MenuFragment extends Fragment implements View.OnClickListener{
    private Button mBtnConvertEnVi;
    private IDisplayMenuScreen mIDisplayMenu;
    private IAtcplTextviewComponents mIAtcplTvComponents;
    private ManagerDatabase mMngDatabase;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViewByIds(view);
        setEvents(view);
        initComponents();
    }

    /**
     * khoi tao gia tri
     *
     */
    public void setValueComponents(IAtcplTextviewComponents iAtcplTvComponents,
                                   IDisplayMenuScreen iDisplayMenu,
                                   ManagerDatabase managerDatabase) {
        mIAtcplTvComponents = iAtcplTvComponents;
        mIDisplayMenu = iDisplayMenu;
        mMngDatabase = managerDatabase;
    }

    private void initComponents() {

        //set lai trang thai cua che do tra tu moi lan tao fragment
        mIAtcplTvComponents.setSearchInEnOrVi(!mIAtcplTvComponents.getSearchInEnOrVi());
        clickButtonConvertEnVi();
    }

    private void setEvents(View view) {
        mBtnConvertEnVi.setOnClickListener(this);
        view.findViewById(R.id.btn_myword).setOnClickListener(this);
        view.findViewById(R.id.btn_history).setOnClickListener(this);
        view.findViewById(R.id.btn_setting).setOnClickListener(this);
        view.findViewById(R.id.btn_exit).setOnClickListener(this);
        view.findViewById(R.id.btn_translate).setOnClickListener(this);
        //su kien an ban phim
        view.findViewById(R.id.scrv_menu).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                StartScreen.hideKeyBoard();
                return false;
            }
        });
    }

    private void findViewByIds(View view) {
        mBtnConvertEnVi = (Button) view.findViewById(R.id.btn_convert_en_vi);
    }

    @Override
    public void onClick(View v) {
        //an ban phim
        StartScreen.hideKeyBoard();

        switch (v.getId()){

            case R.id.btn_convert_en_vi:
                clickButtonConvertEnVi();
                break;

            case R.id.btn_translate:
                clickButtonTranslate();
                break;

            case R.id.btn_myword:
                String nameTable = new DatabaseModel().getMyWordTable().getNameOfTable();
                createListWordsFragment(nameTable);
                break;

            case  R.id.btn_history:
                String nameTable1 = new DatabaseModel().getHistoryTable().getNameOfTable();
                createListWordsFragment(nameTable1);
                break;

            case R.id.btn_setting:
                new SetDialog(this.getContext()).show();
                break;

            case R.id.btn_exit:
                this.getActivity().finish();
                break;

            default:
                break;
        }
    }

    /**
     * tao fragment dich online
     */
    private void clickButtonTranslate() {
        //tao fragment
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        TranslateFragment fragment = new TranslateFragment();
        fragment.setValue(mIDisplayMenu);
        transaction.replace(R.id.frlo_container, fragment);
        transaction.commit();
        //thong bao man hinh menu khong duoc hien thi
        mIDisplayMenu.setIsMenuDisplayed(false);
    }

    /**
     * Tao fragment de hien thi danh sach tu
     * @param nameTable ten bang chua du lieu
     */
    private void createListWordsFragment(String nameTable) {
        //tao fragment
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        ListWordsFragment listWordsFragment = new ListWordsFragment();
        listWordsFragment.setValueComponents( nameTable, mMngDatabase,
                mIDisplayMenu);
        transaction.replace(R.id.frlo_container, listWordsFragment);
        transaction.commit();
        //thong bao man hinh menu khong duoc hien thi
        mIDisplayMenu.setIsMenuDisplayed(false);
    }

    /**
     * chuyen doi che do tra tu
     * en -> vi
     * vi -> en
     */
    private void clickButtonConvertEnVi() {
        if(mIAtcplTvComponents.getSearchInEnOrVi()){
            //chuyen doi sang tr tu tieng viet
            mIAtcplTvComponents.setSearchInEnOrVi(false);
            mBtnConvertEnVi.setText(R.string.search_en);
            mIAtcplTvComponents.setTextHintTextView(R.string.search_vi);
        }else {
            //chuyen doi sang tra tu tieng anh
            mIAtcplTvComponents.setSearchInEnOrVi(true);
            mBtnConvertEnVi.setText(R.string.search_vi);
            mIAtcplTvComponents.setTextHintTextView(R.string.search_en);
        }
    }



}
