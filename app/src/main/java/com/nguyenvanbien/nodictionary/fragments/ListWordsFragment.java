package com.nguyenvanbien.nodictionary.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nguyenvanbien.nodictionary.R;
import com.nguyenvanbien.nodictionary.activities.StartScreen;
import com.nguyenvanbien.nodictionary.database.manager.ManagerDatabase;
import com.nguyenvanbien.nodictionary.interfaces.IDisplayMenuScreen;
import com.nguyenvanbien.nodictionary.interfaces.IListWords;
import com.nguyenvanbien.nodictionary.adapter.AdapterListWord;
import com.nguyenvanbien.nodictionary.word_model.Word;

import java.util.List;


public class ListWordsFragment extends Fragment implements IListWords, View.OnClickListener{
    private String mNameTable;
    private List<Word> mListWords;
    private AdapterListWord mAdtListWords;
    private ListView mLvListWords;
    private ManagerDatabase mMngDatabase;
    private IDisplayMenuScreen mIDisplayMenuScreen;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_words, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViewByIds(view);
        setEvents(view);
        initComponents();
    }

    public void setValueComponents(String nameTable, ManagerDatabase mngDb,
                                   IDisplayMenuScreen iDisplayMenuScreen) {
        mNameTable = nameTable;
        mMngDatabase = mngDb;
        mIDisplayMenuScreen = iDisplayMenuScreen;
    }

    private void findViewByIds(View view){
        mLvListWords = (ListView) view.findViewById(R.id.lv_list_words);
    }

    private void setEvents(View view){
        view.findViewById(R.id.imgbtn_back).setOnClickListener(this);
        view.findViewById(R.id.imgbtn_delete_all).setOnClickListener(this);
        view.findViewById(R.id.tv_result_title).setOnClickListener(this);
        mLvListWords.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                StartScreen.hideKeyBoard();
                return false;
            }
        });
    }

    private void initComponents(){
        mListWords = mMngDatabase.getListWord(mNameTable);
        mAdtListWords = new AdapterListWord( getContext() ,this);
        mLvListWords.setAdapter(mAdtListWords);
    }


    /***************************************
     * OnClickListener
     **************************************/
    @Override
    public void onClick(View v) {
        //an ban phim neu co
        StartScreen.hideKeyBoard();

        switch (v.getId()){
            case R.id.imgbtn_back:
                mIDisplayMenuScreen.displayMenuScreen();
                break;

            case R.id.imgbtn_delete_all:
                deleteAllItem();
                break;

            default:
                break;
        }
    }

    /**
     * IListWords
     */

    @Override
    public int getCount() {
        return mListWords.size();
    }

    @Override
    public Word getItem(int position) {
        return mListWords.get(position);
    }

    @Override
    public void deleteItem(int position) {
        //xoa du lieu trong csdl
        mMngDatabase.deleteWordInTable(mListWords.get(position).mWord, mNameTable);

        //xoa du lieu trong danh sach va thong bao thay doi du lieu
        mListWords.remove(position);
        mAdtListWords.notifyDataSetChanged();
    }

    @Override
    public void deleteAllItem() {
        //xoa du lieu trong danh sach va thong bao thay doi du lieu
        mListWords.clear();
        mAdtListWords.notifyDataSetChanged();

        //xoa du lieu trong csdl
        mMngDatabase.deleteWordInTable(null, mNameTable);
    }

    @Override
    public String[] getWord(String word, String nameTable) {
        return mMngDatabase.getWord(word, nameTable);
    }
}
