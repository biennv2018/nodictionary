package com.nguyenvanbien.nodictionary.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nguyenvanbien.nodictionary.R;
import com.nguyenvanbien.nodictionary.activities.StartScreen;
import com.nguyenvanbien.nodictionary.database.manager.ManagerDatabase;
import com.nguyenvanbien.nodictionary.database.models.DatabaseModel;
import com.nguyenvanbien.nodictionary.interfaces.IDisplayMenuScreen;

public class DisplayWordFragment extends Fragment implements View.OnClickListener{
    private TextView mTvWord, mTvContent;
    private ImageButton mImgbtnLike;
    private ManagerDatabase mMngDatabase;
    private Boolean mLikeWord;
    private Boolean mSearchInEnOrVn;
    private IDisplayMenuScreen mIDisplayMenuScreen;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.display_word, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewByIds(view);
        setEvents(view);
        initComponents();
        setData();
    }

    /**
     * khoi tao du lieu hien thi cho textview
     */
    private void setData() {
        Bundle bundle = getArguments();
        String word = bundle.getString("word");
        String nameTable = bundle.getString("nametable");

        setDataChange(mMngDatabase.getWord(word, nameTable));
    }

    private void findViewByIds(View view){
        mTvWord = (TextView) view.findViewById(R.id.tv_word);
        mTvContent = (TextView) view.findViewById(R.id.tv_content);
        mImgbtnLike = (ImageButton) view.findViewById(R.id.imgbtn_like);
    }

    private void initComponents() {
    }

    private void setEvents(View view) {
        view.findViewById(R.id.imgbtn_back).setOnClickListener(this);
        mImgbtnLike.setOnClickListener(this);
        view.findViewById(R.id.tv_result_title).setOnClickListener(this);
        mTvContent.setOnClickListener(this);
        mTvWord.setOnClickListener(this);
    }

    public void initComponents(ManagerDatabase mngDatabase, Boolean searchInEnOrVn, IDisplayMenuScreen iDisplayMenuScreen) {
        mMngDatabase = mngDatabase;
        mSearchInEnOrVn = searchInEnOrVn;
        mIDisplayMenuScreen = iDisplayMenuScreen;

    }

    /**
     * khoi tao lai du lieu hien thi cho text view
     * @param arrDatas du lieu truyen vao
     */
    public void setDataChange(String[] arrDatas){
        mTvWord.setText(arrDatas[0]);
        mTvContent.setText(arrDatas[1]);
        mLikeWord = mMngDatabase.isWordLiked(arrDatas[0]);
        if(mLikeWord){
            mImgbtnLike.setImageResource(R.drawable.ic_heart_128);
        }else {
            mImgbtnLike.setImageResource(R.drawable.ic_black_heart_128);
        }
    }

    @Override
    public void onClick(View v) {
        //an ban phim
        StartScreen.hideKeyBoard();

        switch (v.getId()){
            case R.id.imgbtn_like:
                clickLikeButton();
                break;

            case R.id.imgbtn_back:
                //hien thi giao dien menu
                mIDisplayMenuScreen.displayMenuScreen();
                break;

            default:
                break;
        }

    }

    /**
     * click vao nut like
     */
    private void clickLikeButton() {
        String nameTable = new DatabaseModel().getMyWordTable().getNameOfTable();
        String word = mTvWord.getText().toString();
        if(mLikeWord){
            //tu dang duoc thich
            //xo khoi tu yeu thich
            mMngDatabase.deleteWordInTable(word, nameTable);
            //thay doi hinh anh
            mImgbtnLike.setImageResource(R.drawable.ic_black_heart_128);
        }else {
            //them vao danh sach tu yeu thich
            String type;
            if(mSearchInEnOrVn){
                type = "en";
            }else {
                type = "vi";
            }
            mMngDatabase.insertWordInTable(word, type , nameTable);
            mImgbtnLike.setImageResource(R.drawable.ic_heart_128);
        }

        //gan lai gia tri
        mLikeWord = !mLikeWord;
    }
}
