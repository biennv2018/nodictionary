package com.nguyenvanbien.nodictionary.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import com.nguyenvanbien.nodictionary.broadcast.NotifyWord;
import com.nguyenvanbien.nodictionary.subclass.SettingFileManager;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.nguyenvanbien.nodictionary.constants.constants.ALARM_MANAGER_BUNDLE_NAME;
import static com.nguyenvanbien.nodictionary.constants.constants.ALARM_MANAGER_NAME;
import static com.nguyenvanbien.nodictionary.constants.constants.ANNOUNCE;

public class NotifyManager extends Service  implements Serializable{
    private transient AlarmManager mDayAlarmManager;
    private AlarmManagerObject mAlarmManagerObject;

    @Override
    public void onCreate() {
        super.onCreate();
        mDayAlarmManager = (AlarmManager) getBaseContext().getSystemService(ALARM_SERVICE);
        mAlarmManagerObject = new AlarmManagerObject(getBaseContext());
    }

    @Override
    public int onStartCommand(Intent intent,  int flags, int startId) {
        //nhan thoi gian bat dau thong bao
        String timeStrat = new SettingFileManager().getTimeStart(getBaseContext());
        //nhan thoi gian timeStart trong ngay hom nay
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        timeStrat = timeStrat+" "+simpleDateFormat.format(date.getTime());
        //dinh dang lai thoi gian
        simpleDateFormat = new SimpleDateFormat("hh:mm dd/MM/yyyy");

        //set bao thuc khi toi thoi diem bat dau nhan thong bao
        Intent myIntent = new Intent(this, NotifyWord.class);
        //them alarmManager vao intent de gui du lieu
        Bundle bundle = new Bundle();
        bundle.putSerializable(ALARM_MANAGER_NAME, mAlarmManagerObject);
        myIntent.putExtra(ALARM_MANAGER_BUNDLE_NAME, bundle);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), 0, myIntent, 0);
        try {
            //moi ngay se bao thuc mot lan
            mDayAlarmManager.setRepeating(
                    AlarmManager.RTC_WAKEUP, simpleDateFormat.parse(timeStrat).getTime(), 86400000, pendingIntent );

            //dat count ve gia tri ban dau
            NotifyWord.count = -1;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * gui du lieu qua intent
     */
    public class AlarmManagerObject implements Serializable{
        private transient AlarmManager mAlarmManager;

        public AlarmManagerObject(Context context) {
            //mAlarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        }

        public void setWakeup(long timeWakeup, Context context){
            Intent intent = new Intent(context, NotifyWord.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
            mAlarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            mAlarmManager.set(AlarmManager.RTC_WAKEUP, timeWakeup, pendingIntent);
        }
    }
}
