package com.nguyenvanbien.nodictionary.services;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import com.nguyenvanbien.nodictionary.broadcast.UnlockScreen;

public class LockService extends Service{
    private UnlockScreen mUnlock;

    @Override
    public void onCreate() {
        super.onCreate();

        registerUnlockReciver();
    }

    private void registerUnlockReciver() {
        mUnlock = new UnlockScreen();
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        registerReceiver(mUnlock, filter);
    }

    @Override
    public int onStartCommand(Intent intent,  int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
