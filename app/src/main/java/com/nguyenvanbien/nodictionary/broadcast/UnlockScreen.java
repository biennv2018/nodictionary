package com.nguyenvanbien.nodictionary.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.nguyenvanbien.nodictionary.activities.LockScreen;
import com.nguyenvanbien.nodictionary.subclass.SettingFileManager;

public class UnlockScreen extends BroadcastReceiver {
    private static final String TAG = UnlockScreen.class.getSimpleName()+">";
    public static boolean UNLOCK = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.d(TAG, "Mở khóa màn hình----------------------------");

        if(UNLOCK){
            return;
        }

        //Log.d(TAG, "Bắt đầu mở khóa màn hình----------------------------");

        UNLOCK = true;

        //kiem tra nguoi dung co cho phep khoa man hinh hay khong
        SettingFileManager setting = new SettingFileManager();
        if(!setting.getLockScreen(context)){
            return;
        }

        //start activity khoa man hinh
        Intent intent1 = new Intent(context, LockScreen.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent1);
    }
}
