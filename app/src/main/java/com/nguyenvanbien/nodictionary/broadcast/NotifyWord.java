/******************************************************************************************
 * Thong bao tu len thanh tac vu
 ****************************************************************************************/
package com.nguyenvanbien.nodictionary.broadcast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import com.nguyenvanbien.nodictionary.R;
import com.nguyenvanbien.nodictionary.activities.LoadingDatabaseScreen;
import com.nguyenvanbien.nodictionary.database.manager.ManagerDatabase;
import com.nguyenvanbien.nodictionary.services.NotifyManager;
import com.nguyenvanbien.nodictionary.subclass.SettingFileManager;
import com.nguyenvanbien.nodictionary.word_model.EnglishWord;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import static com.nguyenvanbien.nodictionary.constants.constants.ALARM_MANAGER_BUNDLE_NAME;
import static com.nguyenvanbien.nodictionary.constants.constants.ALARM_MANAGER_NAME;


public class NotifyWord extends BroadcastReceiver {
    public static int count = -1;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!notifyRandomWord(context, intent)) {
            return;
        }

        //dat thong bao cho lan ke tiep
        setNextNotify(context, intent);
    }

    /**
     * dat thong bao cho lan tiep theo
     * @param context
     * @param intent
     */
    private void setNextNotify(Context context, Intent intent) {
        SettingFileManager settingFileMng = new SettingFileManager();
        int quantity = settingFileMng.getQuantity(context);
        if(quantity==1){
            //neu so luong thong bao bang 1 thi ket thuc
            return;
        }
        String timeStart = settingFileMng.getTimeStart(context);
        String timeEnd = settingFileMng.getTimeEnd(context);
        //xac dinh khoang thoi gian giua 2 lan
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            long space = (sdf.parse(timeEnd).getTime()-sdf.parse(timeStart).getTime())/(quantity-1);

            //xac dinh ngay gio thong bao hien tai
            sdf = new SimpleDateFormat("dd/MM/yyyy");
            long now = System.currentTimeMillis();
            timeStart = timeStart + " " + sdf.format(now);
            timeEnd = timeEnd + " " + sdf.format(now);
            //sua lai dinh dang ngay gio
            sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");

            //xac dinh thoi gian thong bao ke tiep
            //dieu kien: tg lan ke tiep - tg hien tai <= khoang cach giua 2 lan thong bao
            long nextTime = sdf.parse(timeStart).getTime();
            do{
                nextTime = nextTime + space;
            }while (nextTime-now<0 || nextTime-now>space);
            //neu thoi than thong bao lan ke tiep > thoi gian ket thuc thong bao
            //thi ngung thong bao
            if(nextTime>sdf.parse(timeEnd).getTime()){
                return;
            }

            //dat gio thong bao lan ke tiep
            Bundle bundle = intent.getBundleExtra(ALARM_MANAGER_BUNDLE_NAME);

            NotifyManager.AlarmManagerObject alarm =
                    (NotifyManager.AlarmManagerObject) bundle.getSerializable(ALARM_MANAGER_NAME);
            try {
                alarm.setWakeup(nextTime, context);
            }catch (NullPointerException npe){
                npe.printStackTrace();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * thong bao tu ngau nhien
     * @param context ngu canh truyen vao
     */
    private boolean  notifyRandomWord(Context context, Intent myintent) {
        SettingFileManager settingFileMng = new SettingFileManager();
        String timeEnd = settingFileMng.getTimeEnd(context);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String now = sdf.format(System.currentTimeMillis());

        if(count==-1 && !now.equals(settingFileMng.getTimeStart(context))){
            //lan dau bat dau service thi khong thong bao
            count = (count + 1 )%24;
            return true;
        }else {
            count = (count + 1)%24;
        }

        if(now.compareTo(timeEnd)>0 || !settingFileMng.getAnnounceOnOf(context)){
            return false;
        }

        //nhan tu can thong bao
        EnglishWord englishWord = ManagerDatabase.getRandomEnglishWord();
        if(englishWord==null){
            return false;
        }

        //thong bao
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            builder.setSmallIcon(R.drawable.ic_ring);
        }else {
            builder.setSmallIcon(R.drawable.ic_dictionary_128);
        }
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_dictionary_128));
        builder.setContentTitle(englishWord.mWord);
        builder.setContentText(englishWord.mMean);
        builder.setAutoCancel(true);
        //Intent intent = new Intent(context, LoadingDatabaseScreen.class);
        Intent intent = new Intent(context, LoadingDatabaseScreen.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = builder.build();
        manager.notify(count, notification);

        //rung dien thoai neu co
        if(settingFileMng.getVibrateOnOf(context)){
            Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(500);
        }

        return true;
    }

}
