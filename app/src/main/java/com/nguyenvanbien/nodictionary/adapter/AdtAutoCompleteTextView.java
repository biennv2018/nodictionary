package com.nguyenvanbien.nodictionary.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;


public class AdtAutoCompleteTextView extends ArrayAdapter<String> {
    private static final String TAG = AdtAutoCompleteTextView.class.getSimpleName() + ":";
    private Filter mMyFilter;
    private List<String> mItems, mSuggesstion;

    public AdtAutoCompleteTextView(@NonNull Context context, @LayoutRes int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        mItems = objects;
        inits();
    }

    private void inits() {
        mMyFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if(constraint==null || constraint.length()<1){
                    return filterResults;
                }

                mSuggesstion = new ArrayList<>();

                final String strConstraint = constraint.toString().toLowerCase();

                new AsyncTask<Void, Void, Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        //so sanh du lieu
                        try {
                            byte[] bConstraint = strConstraint.getBytes("UTF-8");
                            List<String> datas = new ArrayList<String>(mItems);

                            for (String item : datas) {
                                String tempItem = item.toLowerCase();

                                byte[] bItem = tempItem.getBytes("UTF-8");

                                if(bConstraint.length<=bItem.length){
                                    boolean equal = true;

                                    for(int i=0; i<bConstraint.length; i++){
                                        if(bConstraint[i]!=bItem[i]){
                                            equal = false;
                                            break;
                                        }
                                    }
                                    if(equal){
                                        mSuggesstion.add(item);
                                    }
                                }
                            }
                        }catch (ConcurrentModificationException cme){
                            cme.printStackTrace();
                        }catch(NullPointerException npe){
                            npe.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute();

                filterResults.values = mSuggesstion;
                filterResults.count = mSuggesstion.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                final List<String> fiterList = (List<String>) results.values;
                if(fiterList!=null && fiterList.size()>0) {
                    clear();
                    addAll(fiterList);
                }
                notifyDataSetChanged();
            }
        };

    }

    @NonNull
    @Override
    public Filter getFilter() {

        return mMyFilter;
    }

}
