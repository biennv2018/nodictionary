package com.nguyenvanbien.nodictionary.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nguyenvanbien.nodictionary.R;
import com.nguyenvanbien.nodictionary.activities.StartScreen;
import com.nguyenvanbien.nodictionary.database.models.DatabaseModel;
import com.nguyenvanbien.nodictionary.dialogs.DisplayWordDialog;
import com.nguyenvanbien.nodictionary.interfaces.IListWords;
import com.nguyenvanbien.nodictionary.word_model.Word;

public class AdapterListWord extends BaseAdapter implements View.OnClickListener {
    //context truyen vao dialog
    private Context mContext;
    private IListWords mIListWords;
    private DisplayWordDialog mWordDialog;


    public AdapterListWord(Context context, IListWords iListWords) {
        mContext =  context;
        mIListWords = iListWords;
        mWordDialog = new DisplayWordDialog(mContext);
    }

    @Override
    public int getCount() {
        return mIListWords.getCount();
    }

    @Override
    public Word getItem(int position) {
        return mIListWords.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //khoi tao layout
        if(convertView==null){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_word, parent, false);
            convertView.setTag(new ViewItemWord(convertView, this));

        }

        //set noi dung view
        Word itemWord = getItem(position);
        ViewItemWord viewItemWord = (ViewItemWord) convertView.getTag();
        viewItemWord.mTvWord.setText(itemWord.mWord);
        viewItemWord.mTypeWord = itemWord.mType;
        viewItemWord.posision = position;

        return convertView;
    }

    /**
     * Event onClickListener
     * @param v
     */
    @Override
    public void onClick(View v) {
        //an ban phim neu co
        StartScreen.hideKeyBoard();

        //rootView la view chua view v
        View rootView = (View)v.getTag();
        ViewItemWord viewItemWord = (ViewItemWord) rootView.getTag();

        switch (v.getId()){
            case R.id.tv_word:
                changeViewOnClick(rootView);
                //hien thi tu va nghia
                String nameTable;
                if(viewItemWord.mTypeWord.equals("en")){
                    nameTable = new DatabaseModel().getEnglishWordTable().getNameOfTable();
                }else {
                    nameTable = new DatabaseModel().getVietNamWordTable().getNameOfTable();
                }
                mWordDialog.setDataChange(
                        mIListWords.getWord(viewItemWord.mTvWord.getText().toString(), nameTable));
                mWordDialog.show();
                break;

            case R.id.imgbtn_delete:
                //xoa du lieu va view
                mIListWords.deleteItem(viewItemWord.posision);
                break;

            default:
                break;
        }
    }

    /**
     * thay doi view khi duoc click
     * @param view duoc click
     */
    private void changeViewOnClick(final View view){
        //thay doi mau nen cua view hien tai
        view.setBackgroundResource(R.drawable.shape_background);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setBackgroundResource(R.drawable.shape_rect);
            }
        }, 500);
    }

    /**
     * doi tuong view ItemWord
     */
    class ViewItemWord{
        public TextView mTvWord;
        public String mTypeWord;
        public int posision;
        public ImageButton mImgbtnDelete;

        public ViewItemWord(View view, View.OnClickListener onClickListener) {
            findViewByIds(view);
            setEvents(onClickListener);
            initComponents(view);
        }

        private void initComponents(View rootView) {
            mTvWord.setTag(rootView);
            mImgbtnDelete.setTag(rootView);
        }

        private void findViewByIds(View view){
            mTvWord = (TextView) view.findViewById(R.id.tv_word);
            mImgbtnDelete = (ImageButton) view.findViewById(R.id.imgbtn_delete);
        }

        private void setEvents( View.OnClickListener onClickListener){
            mTvWord.setOnClickListener(onClickListener);
            mImgbtnDelete.setOnClickListener(onClickListener);
        }
    }
}
