/************************************************************************************************
 * Hiển thị thông tin cài đặt
 ***********************************************************************************************/
package com.nguyenvanbien.nodictionary.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.nguyenvanbien.nodictionary.R;
import com.nguyenvanbien.nodictionary.applications.MyApplication;
import com.nguyenvanbien.nodictionary.subclass.SettingFileManager;
import java.util.ArrayList;
import java.util.List;

public class SetDialog extends Dialog implements View.OnClickListener, CompoundButton.OnCheckedChangeListener{
    private Switch mSwchAnnounce, mSwchVibrate, mSwchLock;
    private Spinner mSpnQuantity, mSpnHourStart, mSpnHourEnd, mSpnMinuteStart, mSpnMinuteEnd ;
    private SettingFileManager mSettingFile;

    public SetDialog(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.setting);
        //DisplayMetrics displayMetrics = this.getContext().getResources().getDisplayMetrics();
        //getWindow().setLayout(displayMetrics.widthPixels-50, wrap_content);

        findViewByIds();
        setEvents();
        initComponents();
        contructorValues();
        setVisible();
    }

    /**
     * an hien cac lua chon trong setting
     */
    private void setVisible() {
        int visible;
        if(mSwchAnnounce.isChecked()){
            visible = View.VISIBLE;
        }else {
            visible = View.GONE;
        }

        findViewById(R.id.lnlo_quantity).setVisibility(visible);
        findViewById(R.id.lnlo_end).setVisibility(visible);
        findViewById(R.id.lnlo_start).setVisibility(visible);
        findViewById(R.id.lnlo_vibrate).setVisibility(visible);
    }

    /**
     * khoi tao gia tri mac dinh
     */
    private void contructorValues() {
        //Lay gia tri tu file
        String[] datas = mSettingFile.getData(getContext());

        if(datas[0]!= null && datas[0].equals("true")) {
            mSwchAnnounce.setChecked(true);
        }

        if(datas[1]!=null){
            mSpnQuantity.setSelection(Integer.parseInt(datas[1].substring(0, datas[1].indexOf(" ")))-1);
        }

        if(datas[2]!=null){
            String[] time = datas[2].split(":", 2);
            mSpnHourStart.setSelection(Integer.parseInt(time[0]));
            mSpnMinuteStart.setSelection(Integer.parseInt(time[1]));
        }

        if(datas[3]!=null){
            String[] time = datas[3].split(":", 2);
            mSpnHourEnd.setSelection(Integer.parseInt(time[0]));
            mSpnMinuteEnd.setSelection(Integer.parseInt(time[1]));
        }

        if(datas[4]!= null && datas[4].equals("true")) {
            mSwchVibrate.setChecked(true);
        }

        if(datas[5]!= null && datas[5].equals("true")) {
            mSwchLock.setChecked(true);
        }
    }

    private void initComponents() {
        mSettingFile = new SettingFileManager();

        //tao danh sach
        List<String> quantityList = new ArrayList<>();
        for(int i=1; i<=25; i++){
            quantityList.add(i+" lần");
        }

        List<String> hourList = new ArrayList<>();
        for(int i=0; i<24; i++){
            if(i<10){
                hourList.add("0"+i);
            }else {
                hourList.add(i + "");
            }
        }

        List<String> minuteList = new ArrayList<>();
        for(int i=0; i<60; i++){
            if(i<10){
                minuteList.add("0"+i);
            }else {
                minuteList.add(i+"");
            }
        }

        //set adapter so luong
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getContext(), R.layout.support_simple_spinner_dropdown_item, quantityList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        mSpnQuantity.setAdapter(adapter);

        //set adapter gio
        adapter = new ArrayAdapter<String>(
                getContext(), R.layout.support_simple_spinner_dropdown_item, hourList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        mSpnHourStart.setAdapter(adapter);
        mSpnHourEnd.setAdapter(adapter);

        //set adapter phut
        adapter = new ArrayAdapter<String>(
                getContext(), R.layout.support_simple_spinner_dropdown_item, minuteList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        mSpnMinuteStart.setAdapter(adapter);
        mSpnMinuteEnd.setAdapter(adapter);
    }

    private void findViewByIds() {
        mSwchAnnounce = (Switch) findViewById(R.id.swch_announce);
        mSpnQuantity = (Spinner) findViewById(R.id.spn_quntity);
        mSpnHourStart = (Spinner) findViewById(R.id.spn_hour_start);
        mSpnHourEnd = (Spinner) findViewById(R.id.spn_hour_end);
        mSpnMinuteStart = (Spinner) findViewById(R.id.spn_minute_start);
        mSpnMinuteEnd = (Spinner) findViewById(R.id.spn_minute_end);
        mSwchVibrate = (Switch) findViewById(R.id.swch_vibrate);
        mSwchLock = (Switch) findViewById(R.id.swch_lock);
    }

    private void setEvents() {
        findViewById(R.id.imgbtn_close).setOnClickListener(this);
        findViewById(R.id.btn_save).setOnClickListener(this);
        mSwchAnnounce.setOnCheckedChangeListener(this);
    }


    /**
     * Su kien onClick
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgbtn_close:
                cancel();
                break;

            case R.id.btn_save:
                //kiem tra du lieu truoc khi luu
                if(!checkDataBeforeSave()) return;
                writeDataInSettingFile();
                serviceManager(getContext());
                break;

            default:
                break;
        }
    }

    /**
     * kiem tra du lieu truoc khi luu
     */
    private boolean checkDataBeforeSave() {
        //neu thong bao duoc chon moi check dieu nay
        if(mSwchAnnounce.isChecked()) {
            int hourStart = Integer.parseInt(mSpnHourStart.getSelectedItem() + "");
            int hourEnd = Integer.parseInt(mSpnHourEnd.getSelectedItem() + "");
            if (hourStart > hourEnd) {
                Toast.makeText(getContext(), "Thất bại!", Toast.LENGTH_SHORT).show();
                return false;
            }

            if (hourStart == hourEnd) {
                int minuteStart = Integer.parseInt(mSpnMinuteStart.getSelectedItem() + "");
                int minuteEnd = Integer.parseInt(mSpnMinuteEnd.getSelectedItem() + "");

                if (minuteStart >= minuteEnd) {
                    Toast.makeText(getContext(), "Thất bại!", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * quan ly service
     * bat dau hoac ket thuc service
     */
    private void serviceManager(Context context) {
        MyApplication.startMyServices(context);
    }

    /**
     * ghi du lieu cai dat vao file
     */
    private void writeDataInSettingFile() {
        String[] datas = new String[]{
                Boolean.toString(mSwchAnnounce.isChecked()),
                (String)mSpnQuantity.getSelectedItem(),
                 mSpnHourStart.getSelectedItem()+":"+mSpnMinuteStart.getSelectedItem(),
                mSpnHourEnd.getSelectedItem()+":"+mSpnMinuteEnd.getSelectedItem(),
                Boolean.toString(mSwchVibrate.isChecked()),
                Boolean.toString(mSwchLock.isChecked())
        };

        mSettingFile.setData(getContext(), datas);
        Toast.makeText(getContext(), "Thành công", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        setVisible();
    }

}
