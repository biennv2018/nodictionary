package com.nguyenvanbien.nodictionary.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.nguyenvanbien.nodictionary.R;


public class DisplayWordDialog extends Dialog implements View.OnClickListener{
    private TextView mTvWord, mTvMean;

    public DisplayWordDialog(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.infor_word);
        findViewByIds();
        setEvents();
    }


    private void findViewByIds(){
        mTvWord = (TextView) findViewById(R.id.tv_word);
        mTvMean = (TextView) findViewById(R.id.tv_content);
    }

    private void setEvents(){
        findViewById(R.id.imgbtn_close).setOnClickListener(this);
    }

    public void setDataChange(String[] datas){
        mTvWord.setText(datas[0]);
        mTvMean.setText(datas[1]);
    }

    @Override
    public void onClick(View v) {
        cancel();
    }
}
